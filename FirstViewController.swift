// To Airbnb Software Engineering Apprentice Application hiring manager:
//    Please check line 169 to 206.
//    This function is using delegate to fullfill when the certain category button is clicked, 
//    the program will reload post for chosen category.
//    The delegate protocal is defined in CateBtnCollectionViewCell.swift file.
//
//  FirstViewController.swift
//  ProtoType
//
//  Created by mac on 2017/11/17.
//  Copyright © 2017年 mac. All rights reserved.
//

import UIKit
var SavePage = 0

class FirstViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CategoryChooseDelegate,LikeDelegate,MessageDelegate{

    
    @IBOutlet weak var postCollectionView: UICollectionView!
    @IBOutlet weak var topNavigationBar: UINavigationBar!
    @IBOutlet weak var catChoosingCollectionView: UICollectionView!
    
    let textField = UITextField()
    
    internal var recentPosts: [RecentPost] = []
    internal var imageWithIds: [ImageWithId] = []
    internal var categBtns:[CatButtonInfo] = []
    internal var refresher = UIRefreshControl()
    var category: String = "all"
    internal var loadDataAvailable = true
    
    
    internal func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1 {
            return categBtns.count
        }
        else {
            return recentPosts.count
        }                
    }
    
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "catChooseCollectionViewCell", for: indexPath) as! CatBtnCollectionViewCell
            cell.catBtn.setTitle(categBtns[indexPath.row].labelText, for: .normal)
            if categBtns[indexPath.row].isTapped {
                cell.catBtn.setTappedColor(type: categBtns[indexPath.row].buttonType!)
            } else {
                cell.catBtn.setColor(type: categBtns[indexPath.row].buttonType!)
            }
            cell.delegate = self
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mainPostCell", for: indexPath) as! PostCollectionViewCell
            let lastElement = recentPosts.count - 1
            cell.blogNameLabel.text = recentPosts[indexPath.row].blogName
            cell.postTimeLabel.text = recentPosts[indexPath.row].createTime
            
            let nameText = NSMutableAttributedString(string: recentPosts[indexPath.row].name + "  ", attributes: [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 16)])
            let contentText = NSAttributedString(string: recentPosts[indexPath.row].trimContent, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15)])
            nameText.append(contentText)
            cell.contentLabel.attributedText = nameText
            cell.commentCountLabel.text = String(recentPosts[indexPath.row].messageNum) + "個留言"
            cell.likeCountLabel.text = String(recentPosts[indexPath.row].likeNum) + "個叉"
            cell.photoCountLabel.text = String(recentPosts[indexPath.row].imageCount)
            if imageWithIds[indexPath.row].visited {
                if let image = imageWithIds[indexPath.row].image {
                    cell.firstPhotoImageView.image = image
                } else {
                    cell.firstPhotoImageView.image = UIImage(named: "logoImage")
                }
                
                cell.activityIndicator.stopAnimating()
            } else {
                cell.activityIndicator.startAnimating()
            }
            
            // 設定fork是粉色或線條
            if recentPosts[indexPath.row].like == 1 {
                cell.forkButton.imageView?.image = UIImage(named: "fat_fork_pink")
            } else if recentPosts[indexPath.row].like == 0{
                cell.forkButton.imageView?.image = UIImage(named: "fat_fork_line")
            }
            
            
            if recentPosts[indexPath.row].categoriesInfo.count != 0 {
                let buttonInfo = CatButtonInfo(labelText: recentPosts[indexPath.row].categoriesInfo[0].categoryName, buttonType: CatButton.Categories.userDefined,  number: recentPosts[indexPath.row].categoriesInfo[0].count)
                cell.catButtonInactive.setTitle(buttonInfo.labelTextWithNum!, for: .normal)
                cell.catButtonInactive.setColor(type: CatButton.Categories.userDefined)
                cell.catButtonInactive.sizeToFit()
            } else {
                cell.catButtonInactive.isHidden = true
            }
            
            if indexPath.row == lastElement {
                self.loadMoreData()
            }
            cell.delegate = self
            cell.delegate1 = self
            
            return cell
        }
    
    }
    
    func likeBtnPressed(cell: PostCollectionViewCell) {
        
        let indexPath = self.postCollectionView.indexPath(for: cell)
        
        if recentPosts[(indexPath?.row)!].like == 1 {
            recentPosts[(indexPath?.row)!].likeNum -= 1
            recentPosts[(indexPath?.row)!].like = 0
            cell.forkButton.setImage(UIImage(named: "fat_fork_line"), for: .normal)
        }
        else if recentPosts[(indexPath?.row)!].like == 0{
            recentPosts[(indexPath?.row)!].likeNum += 1
            recentPosts[(indexPath?.row)!].like = 1
            cell.forkButton.setImage(UIImage(named: "fat_fork_pink"), for: .normal)
        }
        
        let postId = recentPosts[(indexPath?.row)!].postId
        let postIdStr = String(describing: postId)
        let urlStr = "http://35.194.208.22/foodle/public/api/posts/like/" + postIdStr
        
        var request = URLRequest(url: URL(string: urlStr)!)
        request.httpMethod = "POST"
        let params: [String: Any] = ["userId": myUserId]
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            if error != nil {
                print("error=\(String(describing: error))")
                return
            }
            
            //Print out response object
            print("response = \(String(describing: response))")
            
            //Convert response sent from a server side script to a NSDictionary object
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                print(data!)
                
                if let parseJSON = json {
                    // Access value of first name by its key
                    let postId = parseJSON["like"] as? Int
                    print("likeResponse: \(String(describing: postId))")
                }
            } catch {
                print(error)
            }
        }
        task.resume()
        
        postCollectionView.reloadData()
        
    }
    
    func messageBtnPressed(cell: PostCollectionViewCell) {
        let indexPath = self.postCollectionView.indexPath(for: cell)
        let info = recentPosts[(indexPath?.row)!]
        performSegue(withIdentifier: "postToMessage", sender: info)
        
    }
    
    // Updated posts when certain category button is clicked.
    func catBtnTapped(cell: CatBtnCollectionViewCell) {
        let indexPath = self.catChoosingCollectionView.indexPath(for: cell)
        let btnInfo = categBtns[(indexPath?.row)!]
        let btn = cell.catBtn!
        if btnInfo.isTapped {
            if (indexPath?.row)! != 0 {
                btn.resetColor(type: categBtns[(indexPath?.row)!].buttonType!)
                categBtns[(indexPath?.row)!].isTapped = false
                categBtns[0].isTapped = true
                category = "all"
                DispatchQueue.main.async {
                    self.catChoosingCollectionView.reloadData()
                }
                getData()
            }

        } else {
            for i in categBtns {
                i.isTapped = false
            }
            categBtns[(indexPath?.row)!].isTapped = true

            if (indexPath?.row)! == 0 {
                category = "all"
            } else {
                category = String(describing: (btnInfo.categoryId!))
            }
            DispatchQueue.main.async {
                self.catChoosingCollectionView.reloadData()
            }
            getData()
        }


        let indexPathScroll = NSIndexPath(row: 0, section: 0)
        postCollectionView.scrollToItem(at: indexPathScroll as IndexPath, at: .bottom, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        category = "all"
        loadCatButtonData()
        getData()
        refresher.attributedTitle = NSAttributedString(string: "載入中")
        refresher.addTarget(self, action: #selector(FirstViewController.getData), for: UIControlEvents.valueChanged)
        postCollectionView.addSubview(refresher)
        postCollectionView.delegate = self
        postCollectionView.dataSource = self
        topNavigationBar.setBackgroundImage(UIColor.clear.as1ptImage(), for: .default)
        topNavigationBar.shadowImage = UIColor(red:1.00, green:0.00, blue:0.00, alpha:0.4).as1ptImage()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if categBtns.count == 0 {
            categBtns.append(CatButtonInfo(labelText: "全部", buttonType: CatButton.Categories.system, categoryId: 0))
            categBtns[0].isTapped = true
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func getData(){
        loadDataAvailable = true
        var tmpPosts: [RecentPost] = []
        var tmpImgs: [ImageWithId] = []
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmss"
        formatter.timeZone = TimeZone(abbreviation: "HKT")
        let str = formatter.string(from: Date())
        let urlStr = "http://35.194.208.22/foodle/public/api/posts/recentPosts/\(myUserId)/" + str + "/3/" + category
        
        var request = URLRequest(url: URL(string: urlStr)!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            guard let data = data else { return}
            do {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                tmpPosts = try JSONDecoder().decode([RecentPost].self, from: data)
                self.recentPosts = tmpPosts
                for i in 0...(self.recentPosts.count - 1) {
                    tmpImgs.append(ImageWithId(imageId: String(describing:(self.recentPosts[i].coverImageId))))
                    self.recentPosts[i].blogName = self.recentPosts[i].blogName.decodeEmoji
                    self.recentPosts[i].trimContent = self.recentPosts[i].trimContent.decodeEmoji
                }
                self.imageWithIds = tmpImgs
                DispatchQueue.main.async {
                    self.refresher.endRefreshing()
                    self.postCollectionView.reloadData()
                }
                self.getImage()
            } catch let jsonErr {
                print("FirstViewController-getData(): Error serializing json:", jsonErr)
            }
        }
        task.resume()
    }
    
    internal func loadMoreData() {
        
        if !loadDataAvailable {
            return
        }
        loadDataAvailable = false
        var time = recentPosts[recentPosts.endIndex - 1].createTime
        time = time.replacingOccurrences(of: "[^0-9]", with: "", options: [.regularExpression])
        var request = URLRequest(url: URL(string: "http://35.194.208.22/foodle/public/api/posts/recentPosts/\(myUserId)/" + time + "/5/" + category)!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            guard let data = data else { return}
            do {
                let newData = try JSONDecoder().decode([RecentPost].self, from: data)
                if newData.count == 0 {
                    self.loadDataAvailable = false
                } else {
                    self.recentPosts.append(contentsOf: newData)
                    for i in newData {
                        self.imageWithIds.append(ImageWithId(imageId: String(describing:(i.coverImageId))))
                    }
                    DispatchQueue.main.async {
                        self.postCollectionView.reloadData()
                        self.refresher.endRefreshing()
                    }
                    
                    self.loadDataAvailable = true
                    
                    self.getImage()
                }
            } catch let jsonErr {
                print("First view loadMoreData -- Error serializing json:", jsonErr)
                self.loadDataAvailable = false
            }
        }
        task.resume()
    }
    
    func getImage() {
        let dataUrl = "http://35.194.208.22/foodle/public/api/image/"
        
        for i in imageWithIds {
            if !i.visited {
                let imageUrl = dataUrl + i.imageId
                var request = URLRequest(url: URL(string: imageUrl)!)
                request.httpMethod = "GET"
                let task = URLSession.shared.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
                    guard let data = data else { return}
                    if let response = response as? HTTPURLResponse{
                        if response.statusCode == 200 {
                            i.visited = true
                        }
                    }
                    do {
                        let imageData = try JSONDecoder().decode(ImageInfo.self, from: data)
                        let image = UIImage(data: NSData(base64Encoded: imageData.imgData, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)! as Data)
                        if image != nil {
                            i.setImage(image: (image as UIImage?)!)
                        }
                        DispatchQueue.main.async {
                            self.postCollectionView.reloadData()
                        }
                    } catch let jsonErr {
                        print("-----firstView getImage() Error serializing json:-----")
                        print(imageUrl)
                        print(jsonErr)
                    }
                }
                task.resume()
            }
        }
    }
 
    internal func loadCatButtonData() {
        var request = URLRequest(url: URL(string: "http://35.194.208.22/foodle/public/api/category/recentCategories/\(myUserId)")!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data:Data?, response:URLResponse?, error:Error?) in
            guard let data = data else { return}
            do {
                let defaltCategories = try JSONDecoder().decode([DefaultCategory].self, from: data)
                for i in defaltCategories {
                    self.categBtns.append(CatButtonInfo(labelText: i.categoryName, buttonType: CatButton.Categories.system, categoryId: i.categoryId))
                }
                DispatchQueue.main.async {
                    self.catChoosingCollectionView.reloadData()
                }
            } catch let jsonErr {
                print("-----firstView loadCatButtonData() Error serializing json: -----", jsonErr)
            }
        }
        task.resume()
    }
    
    // Rearrange cell size
    // tag 0: post
    // tag 1: btn
    fileprivate let sectionInsets = UIEdgeInsets(top: 0, left: 2, bottom: 0.0, right: 2.0)
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0 {
            return CGSize(width: collectionView.bounds.size.width, height: 550.0)
        } else {
            //2
            let size: CGSize = categBtns[indexPath.row].labelText.size(withAttributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0)])
            return CGSize(width: size.width + 20, height: catChoosingCollectionView.bounds.size.height)
        }
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    //傳postId到postPageViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FirstSeguePush" {
            if let indexPath = postCollectionView.indexPathsForSelectedItems {
                let vc = segue.destination as! PostPageViewController
                vc.postId = String(recentPosts[indexPath[0].row].postId)
            }
        }
        else if segue.identifier == "postToMessage" {
            let info = sender as! RecentPost
            let vc = segue.destination as! Message1ViewController
            vc.postId = String(info.postId)
            vc.userName = String(info.name)
            vc.userMessage = String(info.trimContent)
            vc.userTimeStr = String(info.createTime)
            vc.userImageId = String(info.userImageId)
        }
    }
}

