//
//  CateBtnCollectionViewCell.swift
//  ProtoType
//
//  Created by Chingan on 25/11/2017.
//  Copyright © 2017 mac. All rights reserved.
//

import UIKit

class CatBtnCollectionViewCell: UICollectionViewCell {
    @IBOutlet var catBtn: CatButton!
    
    //2. create delegate variable
    var delegate: CategoryChooseDelegate?
    
    //3. assign this action to category button
    @IBAction func catBtnTapped(sender: AnyObject){
        //4. call delegate method
        //check delegate is not nil
        if let _ = delegate {
            delegate?.catBtnTapped(cell: self)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
}

//1. Define a delegate protocol
protocol CategoryChooseDelegate{
    func catBtnTapped(cell: CatBtnCollectionViewCell)
}

